from eelab import Simulation
import matplotlib.pyplot as plt
import numpy as np
from eelab.dtypes import VolData
import matplotlib.colors as c

s1f=Simulation('./N2E13D.5_C3_V1_full')
s2f=Simulation('./N2E13D.5_C3_V2_full')
s3f=Simulation('./N2E13D.5_C3_V3')
s4f=Simulation('./N2E13D.5_C3_V4_full')
s5f=Simulation('./N2E13D.5_C3_V5_full')

niN1_3f=s3f['niN1']
niN2_3f=s3f['niN2']
niN3_3f=s3f['niN3']
niN4_3f=s3f['niN4']
niN5_3f=s3f['niN5']
niN6_3f=s3f['niN6']
niN7_3f=s3f['niN7']

niN1_3f.values+=niN2_3f.values+niN3_3f.values+niN4_3f.values+niN5_3f.values+niN6_3f.values+niN7_3f.values

fig,ax=plt.subplots()
niN1_3f.plot('rzslice',12)
ax.set_title('Total Nitrogen Density')
plt.scatter(536.28,98.84, c='red', marker='x')
plt.show()

niNfl_3f=niN1_3f.fieldline(536.28,98.84,12.0)

fig,ax=plt.subplots()
niNfl_3f.plot()
#ax.set_xlim(-500,0)
ax.set_title('Total Nitrogen Density')
plt.show()
