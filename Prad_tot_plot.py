from eelab import Simulation
import matplotlib.pyplot as plt
import numpy as np
from eelab.dtypes import VolData
from eelab import Geometry

#Plot total radiation from all five valves at specified toroidal angle

S1=Simulation('./N2E13D.5_C3_V1_full')
S2=Simulation('./N2E13D.5_C3_V2_full')
S3=Simulation('./N2E13D.5_C3_V3_full')
S4=Simulation('./N2E13D.5_C3_V4_full')
S5=Simulation('./N2E13D.5_C3_V5_fullnew')

Prad1=S1['Seecool']
Prad2=S2['Seecool']
Prad3=S3['Seecool']
Prad4=S4['Seecool']
Prad5=S5['Seecool']

Prad1.values+=Prad2.values+Prad3.values+Prad4.values+Prad5.values

#specify angle here
Prad1_36=Prad1.rzslice(36)

fig,ax=plt.subplots()
tmp=ax.pcolormesh(Prad1_36.grid.u/100, Prad1_36.grid.v/100, Prad1_36.values,cmap ='viridis_r')
cbar=plt.colorbar(tmp)
cbar.set_label(r'Seecool [$\frac{W}{cm^3}$]')
ax.set_xlabel('Major Radius [m]')
ax.set_ylabel('Vertical Direction [m]')
plt.show()
