from eelab import Simulation
import matplotlib.pyplot as plt
import numpy as np
from eelab.dtypes import VolData

# load simulation
S=Simulation('./N2E13D.5_C3_V3_full')
Prad=S['Seecool']

# plot radiation at given toroidal angle
Prad_36=Prad.rzslice(36)
fig,ax=plt.subplots()
tmp=ax.pcolormesh(Prad_36.grid.u,Prad_36.grid.v,Prad_36.values)
cbar=plt.colorbar(tmp)
cbar.set_label(r'Seecool [$\frac{W}{cm^3}$]')
ax.set_xlabel('Major Radius [cm]')
ax.set_ylabel('Vertical Direction [cm]')
ax.set_title(r'Triangular plane at $36^\circ$')
plt.show()

# field line tracing of radiation
Pradfl=Prad.fieldline(534.78,96.73,12.0)

fig,ax=plt.subplots()
Pradfl.plot()
plt.show()

# define the grid from the fieldline tracing
x=Pradfl.grid.x1
y=Pradfl.grid.x2
z=Pradfl.grid.x3

dist=0
i=1

# calculate cumulative arc length along the field line
# stop cumulative arc length calculation when distance reaches chosen value
# chosen value is determined from x-axis of field line plot
# absolute difference between first x value in the plot and x value to trace to
# grid is in cm and plot is in m so multiply this by 100

while dist < 17792:
    dist += np.sqrt((x[i+1]-x[i])**2 + (y[i+1]-y[i])**2 + (z[i+1]-z[i])**2)
    i+=1

# Print location of tracing result
print('x: ', x[i-1])
print('y: ', y[i-1])
print('z: ', z[i-1])

# Print toroidal angle of tracing result
Phi=np.arctan2(y[i-1],x[i-1])
Phi=(Phi*180/np.pi)
print('Phi: ', Phi)

# Toroidal angle may not be in simulation domain - change 1 to ensure phi is
# within simulation domain
Phi = Phi + (72*1)
Prad_Decay=Prad.rzslice(Phi)
# convert to the r position of the tracing result
r_decay=np.sqrt(x[i-1]**2 + y[i-1]**2)

# Plot tracing result: toroidal angle plotted with red x at the
# trace result location
fig,ax=plt.subplots()
tmp=ax.pcolormesh(Prad_Decay.grid.u, Prad_Decay.grid.v, Prad_Decay.values, cmap='viridis_r')
cbar=plt.colorbar(tmp)
plt.scatter(r_decay, z[i-1], c='red', marker='x')
cbar.set_label(r'Seecool [$\frac{W}{cm^3}$]')
ax.set_xlabel('Major Radius [cm]')
ax.set_ylabel('Vertical Direction [cm]')
Phi=Phi- (72*1)
ax.set_title('Toroidal Angle: %f' %Phi)


plt.show()
