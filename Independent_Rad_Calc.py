from eelab import Simulation
import matplotlib.pyplot as plt
import numpy as np
from eelab.dtypes import VolData
from eelab import Geometry
import atomdat.adas.adf11 as adf11
import atomdat.adas as adas

## Use plt96_n.dat file to calculate the total nitrogen
## and carbon radiation independently

# Load Simulation
S=Simulation('./N2E13D.5_C3_V3_full')

# Load plt96_n file
fn='plt96_n.dat'


# Plasma Density
ni=S['ni']
# Plasma Temperature
Te=S['Te']
# Total Radiation
Seecool=S['Seecool']


#Number of ionisation states
nstate_nitrogen=7


def get_lz(fn, ni, Te,i):
    # Get Radiative Power Loss Coefficient
     # Inputs:
     #    fn: Filename for plt96 file
     #    ni: Plasma density
     #    Te: Temperature
     #    i:  index referring to current ionisation state
 
    mydat=adf11.ADF11()
    print(fn)
    mydat.read(fn)
    lz=mydat.interpolate(ni*(1e6),Te,i)

    return lz

# Initialise Lz coefficient and prad
Lz=np.zeros((len(ni.values), nstate_nitrogen))
prad_n=np.zeros((len(ni.values)))

# Loop over each ionisation state to calculate prad for nitrogen
for i in range(nstate_nitrogen):
    print(i)
    nz=S['niN'+str(i+1)]
    Lz[:,i]=get_lz(fn,ni.values,Te.values, i)
    prad_n+=np.multiply(Lz[:,i],np.multiply(ni.values*(1e6),nz.values*(1e6)))


nstate_carbon=6

# Load plt96_c file
fn='plt96_c.dat'

ni=S['ni']
Te=S['Te']

# Initialise Lz coefficient and prad
Lz=np.zeros((len(ni.values), nstate_carbon))
prad_c=np.zeros((len(ni.values)))

# Loop over each ionisation state to calculate prad for carbon
for i in range(nstate_carbon):
    print(i)
    nz=S['niC'+str(i+1)]
    Lz[:,i]=get_lz(fn,ni.values,Te.values,i)
    prad_c+=np.multiply(Lz[:,i],np.multiply(ni.values*(1e6),nz.values*(1e6)))

    
# Put independent radiation data into VolData objects
Prad_n=VolData(values=prad_n*(1e-6),grid=Seecool.grid,rank='scalar',symbol='Prad_N')
Prad_c=VolData(values=prad_c*(1e-6),grid=Seecool.grid,rank='scalar',symbol='Prad_C')
Prad_tot=VolData(values=(prad_n+prad_c)*(1e-6),grid=Seecool.grid,rank='scalar',symbol='Prad_tot')

#np.savetxt('Nitrogen_Radiation',prad_n)
#np.savetxt('Carbon_Radiation',prad_c)

print(dir(Prad_n))

# Plot Radiation
Prad_n_36=Prad_n.rzslice(36)
fig,ax=plt.subplots()
tmp=ax.pcolormesh(Prad_n_36.grid.u/100, Prad_n_36.grid.v/100, Prad_n_36.values)
cbar=plt.colorbar(tmp)
cbar.set_label(r'Prad [$\frac{W}{cm^3}$]')
ax.set_xlabel('Major Radius [m]')
ax.set_ylabel('Vertical Direction [m]')
ax.set_title('Nitrogen')

Prad_c_36=Prad_c.rzslice(36)
fig,ax=plt.subplots()
tmp=ax.pcolormesh(Prad_c_36.grid.u/100, Prad_c_36.grid.v/100, Prad_c_36.values)
cbar=plt.colorbar(tmp)
cbar.set_label(r'Prad [$\frac{W}{cm^3}$]')
ax.set_xlabel('Major Radius [m]')
ax.set_ylabel('Vertical Direction [m]')
ax.set_title('Carbon')

Prad_tot_36=Prad_tot.rzslice(36)
fig,ax=plt.subplots()
tmp=ax.pcolormesh(Prad_tot_36.grid.u/100, Prad_tot_36.grid.v/100, Prad_tot_36.values)
cbar=plt.colorbar(tmp)
cbar.set_label(r'Prad [$\frac{W}{cm^3}$]')
ax.set_xlabel('Major Radius [m]')
ax.set_ylabel('Vertical Direction [m]')
ax.set_title('Total Calculated Radiation')
plt.show()
