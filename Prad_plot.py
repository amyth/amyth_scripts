from eelab import Simulation
import matplotlib.pyplot as plt
import numpy as np
from eelab.dtypes import VolData
from eelab import Geometry

#Plot radiation from single valve at specified toroidal angle

S=Simulation('./N2E13D.5_C3_V3_full')

Prad=S['Seecool']

#Specifiy toroidal angle here
Prad_36=Prad.rzslice(36)

fig,ax=plt.subplots()
tmp=ax.pcolormesh(Prad_36.grid.u/100, Prad_36.grid.v/100, Prad_36.values,cmap ='viridis_r')
cbar=plt.colorbar(tmp)
cbar.set_label(r'Seecool [$\frac{W}{cm^3}$]')
ax.set_xlabel('Major Radius [m]')
ax.set_ylabel('Vertical Direction [m]')
ax.set_title('Radiation from Simulation')
plt.show()
