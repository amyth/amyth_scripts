import numpy as np
import matplotlib.pyplot as plt


#Import saved files - from volume integration
V1=np.loadtxt('V1_C_full_Rad_Vol_trace_ind', delimiter=',')
Trace3=np.loadtxt('V3_C_Rad_Vol_trace', delimiter=',')
V2=np.loadtxt('V2_C_full_Rad_Vol_trace_ind', delimiter=',')
V3=np.loadtxt('V3_C_full_Rad_Vol_trace_ind', delimiter=',')
V4=np.loadtxt('V4_C_full_Rad_Vol_trace_ind', delimiter=',')
V5=np.loadtxt('V5_C_full_Rad_Vol_trace_ind', delimiter=',')

#Plot seeding angle
valve_loc_y=np.linspace(0,3500,100)
valve_loc_x=np.zeros((len(valve_loc_y)))
valve_loc_x+=12


X=np.loadtxt('Rad_dist_X', delimiter=',')
fig,ax =plt.subplots()
ax.scatter(X,abs(Trace3), color='black', label='Trace, Valve 3')
ax.scatter(X,V1, color='blue', label='Non-trace, Valve 1')
ax.scatter(X,V2, color='red', label='Non-trace, Valve 2')
ax.scatter(X,V3, color='green', label='Non-trace, Valve 3')
ax.scatter(X,V4, color='orange', label='Non-trace, Valve 4')
ax.scatter(X,V5, color='purple', label='Non-trace, Valve 5')

ax.plot(valve_loc_x, valve_loc_y, color='black', label='Seeding Location', linestyle='--')
ax.set_xlabel('Toroidal Angle')
ax.set_ylabel('Volume Integrated Radiation [W]')
ax.grid()
ax.legend(ncols=2)
ax.set_ylim(-100,500)

plt.show()

#Ratios between trace and non-trace simulations (Carbon radiation)

Trace1=np.loadtxt('V1_C_Rad_Vol_trace', delimiter=',')
Trace2=np.loadtxt('V2_C_Rad_Vol_trace', delimiter=',')
Trace4=np.loadtxt('V4_C_Rad_Vol_trace', delimiter=',')
Trace5=np.loadtxt('V5_C_Rad_Vol_trace', delimiter=',')

R1=abs(V1/Trace1)
R2=abs(V2/Trace2)
R3=abs(V3/Trace3)
R4=abs(V4/Trace4)
R5=abs(V5/Trace5)


fig,ax=plt.subplots()
ax.scatter(X,R1,label='Valve 1',color='blue')
ax.scatter(X,R2,label='Valve 2',color='red')
ax.scatter(X,R3,label='Valve 3',color='green')
ax.scatter(X,R4,label='Valve 4',color='orange')
ax.scatter(X,R5,label='Valve 5',color='purple')
ax.legend()
ax.set_xlabel('Toroidal Angle')
ax.set_ylabel('Ratio')
ax.set_title('Ratio Between Carbon Radiation in Trace and Non-Trace Simulations')
ax.grid()
plt.show()
