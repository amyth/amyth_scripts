# Since the neutral density and ion densities have different domains
# the domains must be made equal before total density can be calculated
# the entire plasma domain from volumetric data from simulation results
# can be put into one array which is the entire domain: including the
# extended area where the neutrals extend to


from eelab import Simulation
import matplotlib.pyplot as plt
import numpy as np
from eelab.dtypes import VolData
from eelab import Geometry

# Import Simulation and geometry
s=Simulation('./N2E13D.5_C3_V3_full')
g=Geometry('./../geometry')

## Load Neutral and Ion Density Data
niN1=s['niN1']
niN2=s['niN2']
niN3=s['niN3']
niN4=s['niN4']
niN5=s['niN5']
niN6=s['niN6']
niN7=s['niN7']

niC1=s['niC1']
niC2=s['niC2']
niC3=s['niC3']
niC4=s['niC4']
niC5=s['niC5']
niC6=s['niC6']

n0C=s['n0C']

nH=s['nH']
nH2=s['nH2']
nH2pl=s['nH2+']
niH1=s['niH1']

fig,ax=plt.subplots()
nH.plot('rzslice',12)
#plt.show()


## Put entire plasma domain into one array
def Get_Domain(dat):
    idcell = dat.grid.idcell
    ntor = g.srf_toro[0]; npol = g.srf_polo[0]; nrad=g.srf_radi[0]
    dat_vol = np.zeros(np.shape(idcell))
    ind = np.where(idcell < dat.grid.nc_pl)
    dat_vol[ind]=dat.values[idcell[ind]]
    dat_r = np.reshape(dat.grid.rg,(ntor,npol,nrad))
    dat_z = np.reshape(dat.grid.zg,(ntor,npol,nrad))
    dat_phi=dat.grid.phi_plane
    dat_vol=np.reshape(dat_vol,(ntor-1,npol-1,nrad-1))
    return dat_r, dat_z, dat_phi, dat_vol


nH_r, nH_z, nH_phi, nH_vol = Get_Domain(nH)

niN1_r, niN1_z, niN1_phi, niN1_vol = Get_Domain(niN1)
niN2_r, niN2_z, niN2_phi, niN2_vol = Get_Domain(niN2)
niN3_r, niN3_z, niN3_phi, niN3_vol = Get_Domain(niN3)
niN4_r, niN4_z, niN4_phi, niN4_vol = Get_Domain(niN4)
niN5_r, niN5_z, niN5_phi, niN5_vol = Get_Domain(niN5)
niN6_r, niN6_z, niN6_phi, niN6_vol = Get_Domain(niN6)
niN7_r, niN7_z, niN7_phi, niN7_vol = Get_Domain(niN7)

niC1_r, niC1_z, niC1_phi, niC1_vol = Get_Domain(niC1)
niC2_r, niC2_z, niC2_phi, niC2_vol = Get_Domain(niC2)
niC3_r, niC3_z, niC3_phi, niC3_vol = Get_Domain(niC3)
niC4_r, niC4_z, niC4_phi, niC4_vol = Get_Domain(niC4)
niC5_r, niC5_z, niC5_phi, niC5_vol = Get_Domain(niC5)
niC6_r, niC6_z, niC6_phi, niC6_vol = Get_Domain(niC6)

n0C_r, n0C_z, n0C_phi, n0C_vol = Get_Domain(n0C)

nH2_r, nH2_z, nH2_phi, nH2_vol = Get_Domain(nH2)

nH2pl_r, nH2pl_z, nH2pl_phi, nH2pl_vol = Get_Domain(nH2pl)

niH1_r, niH1_z, niH1_phi, niH1_vol =Get_Domain(niH1)

nH_vol += niN1_vol + niN2_vol + niN3_vol + niN4_vol + niN5_vol + niN6_vol + niN7_vol + niC1_vol + niC2_vol + niC3_vol + niC4_vol + niC5_vol + niC6_vol + n0C_vol + nH2_vol + nH2pl_vol + niH1_vol

fig,ax = plt.subplots()
c = ax.pcolormesh(nH_r[48,:,:],nH_z[48,:,:],nH_vol[48,:,:], cmap='viridis_r', edgecolor='face')
ax.set_title(nH_phi[48])
ax.set_xlabel('Major Radius [cm]')
ax.set_ylabel('Vertical Direction [cm]')
fig.colorbar(c,ax=ax,label=r'Density [$\frac{1}{cm^3}$]')



plt.show()

