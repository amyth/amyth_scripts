from eelab import Simulation
import matplotlib.pyplot as plt
import numpy as np
from eelab.dtypes import VolData
from eelab import Geometry

# Import Simulation and geometry
S=Simulation('./N2E13D.5_C3_V3_full')
g=Geometry('./../geometry')

# Extract Prad data from simulation
Seecool=S['Seecool']

## Put entire plasma domain into one array
idcell=Seecool.grid.idcell
ntor = g.srf_toro[0]; npol=g.srf_polo[0]; nrad=g.srf_radi[0]
Seecool_volume=np.zeros(np.shape(idcell))
# There are areas where idcell is larger than total number of plasma cells
# which would throw up errors when trying to get Seecool.values[idcell]
ind=np.where(idcell < Seecool.grid.nc_pl)
Seecool_volume[ind]=Seecool.values[idcell[ind]]
r=np.reshape(Seecool.grid.rg,(ntor,npol,nrad))
z=np.reshape(Seecool.grid.zg,(ntor,npol,nrad))
phi=Seecool.grid.phi_plane
Seecool_volume=np.reshape(Seecool_volume,(ntor-1,npol-1,nrad-1))
##

#If doing volume integration of entire domain set to True
Full_domain=False

#Plot specified toroidal angle (Specified by index rather than angle itself)
fig,ax=plt.subplots()
c=ax.pcolormesh(r[24,:,:],z[24,:,:],Seecool_volume[24,:,:],cmap='viridis_r',edgecolor='face')
ax.set_title(Seecool.grid.phi_plane[24])
ax.set_xlabel('Major Radius [cm]')
ax.set_ylabel('Vertical Direction [cm]')
fig.colorbar(c,ax=ax,label=r'Seecool [$\frac{W}{cm^3}$]')

## Start defining area for integration
if Full_domain == False:

    tot=0
    i_list = np.zeros(0)
    j_list = np.zeros(0)

#V5: 530,543,90,101
#536
#larger area: 505,545,94,99
#Carbon radiation from triangular plane: 543, 580, 20, 45
#Plot for power point upper divertor all 5 valves: 533,553,90,91

# The grid is defined radially and poloidally, however r and z arrays contain r
# and z information so define area based on this
    for i in range(len(Seecool_volume[24,0,:])):
        for j in range(len(Seecool_volume[24,:,0])):
            if r[48,j,i] > 530 and r[48,j,i] < 543 and z[48,j,i] > 90 and z[48,j,i] < 101:
                tot+=1
                i_list=np.append(i_list,i)
                j_list=np.append(j_list,j)

# Find index values that define the largest area based on the above definitions
    rmin=int(np.min(i_list))
    rmax=int(np.max(i_list))
    zmin=int(np.min(j_list))
    zmax=int(np.max(j_list))
# Take slices out of the arrays based on the indices defined above
    Seecool_volume=Seecool_volume[:,zmin:zmax,rmin:rmax]
    r=r[:,zmin:zmax+1,rmin:rmax+1]
    z=z[:,zmin:zmax+1,rmin:rmax+1]
##

fig,ax=plt.subplots()
c=ax.pcolormesh(r[48,:,:],z[48,:,:],Seecool_volume[48,:,:],cmap='viridis_r',edgecolor='face')
ax.set_title(Seecool.grid.phi_plane[48])
ax.set_xlabel('Major Radius [cm]')
ax.set_ylabel('Vertical Direction [cm]')
fig.colorbar(c,ax=ax,label=r'Seecool [$\frac{W}{cm^3}$]')

fig,ax=plt.subplots()
c=ax.pcolormesh(r[71,:,:],z[71,:,:],Seecool_volume[71,:,:],cmap='viridis_r',edgecolor='face')
ax.set_title(Seecool.grid.phi_plane[72])
ax.set_xlabel('Major Radius [cm]')
ax.set_ylabel('Vertical Direction [cm]')
fig.colorbar(c,ax=ax,label=r'Seecool [$\frac{W}{cm^3}$]')
plt.show()

## Start calculating volumes
#Step 1: Convert to cartesian coordinates
x=np.zeros(np.shape(r)); y=np.zeros(np.shape(r))

for i in range(len(phi)):
    x[i,:,:]=np.multiply(r[i,:,:],np.cos(phi[i]*np.pi/180))
    y[i,:,:]=np.multiply(r[i,:,:],np.sin(phi[i]*np.pi/180))

my_volcel=np.zeros((ntor-1,len(r[0,:,0])-1,len(r[0,0,:])-1))

def tetrahedron_volume(v1,v2,v3,v4):
    matrix=np.array([
        [v1[0], v1[1], v1[2],1],
        [v2[0], v2[1], v2[2],1],
        [v3[0], v3[1], v3[2],1],
        [v4[0], v4[1], v4[2],1]])
    det=np.linalg.det(matrix)
    volume=abs(det)/6
    return volume

#Step 2: Calculate cell volumes
for i in range(ntor-1):
    print('Started toroidal index %i' %i)

    for j in range(len(r[0,:,0])-1):
        for k in range(len(z[0,0,:])-1):
            x1=x[i,j,k]; x2=x[i+1,j,k]; x3=x[i+1,j+1,k]; x4=x[i,j+1,k]; x5=x[i,j,k+1]; x6=x[i+1,j,k+1]; x7=x[i+1,j+1,k+1]; x8=x[i,j+1,k+1]
            y1=y[i,j,k]; y2=y[i+1,j,k]; y3=y[i+1,j+1,k]; y4=y[i,j+1,k]; y5=y[i,j,k+1]; y6=y[i+1,j,k+1]; y7=y[i+1,j+1,k+1]; y8=y[i,j+1,k+1]
            z1=z[i,j,k]; z2=z[i+1,j,k]; z3=z[i+1,j+1,k]; z4=z[i,j+1,k]; z5=z[i,j,k+1]; z6=z[i+1,j,k+1]; z7=z[i+1,j+1,k+1]; z8=z[i,j+1,k+1]
            
            v1=np.array([x1,y1,z1]); v2=np.array([x2,y2,z2]); v3=np.array([x3,y3,z3]); v4=np.array([x4,y4,z4])
            v5=np.array([x5,y5,z5]); v6=np.array([x6,y6,z6]); v7=np.array([x7,y7,z7]); v8=np.array([x8,y8,z8])

            #tetrahedron 1: indices 1, 2, 4, and 5
            vt1=tetrahedron_volume(v1,v2,v4,v5)

            #tetrahedron 2: indices 2, 3, 4 and 7
            vt2=tetrahedron_volume(v2,v3,v4,v7)

            #tetrahedron 3: indices 2, 4, 5, and 7
            vt3=tetrahedron_volume(v2,v4,v5,v7)

            #tetrahedron 4: indices 5, 6, 7, and 2
            vt4=tetrahedron_volume(v5,v6,v7,v2)

            #tetrahedron 5: indices 4, 5, 7, and 8
            vt5=tetrahedron_volume(v4,v5,v7,v8)

            my_volcel[i,j,k]=vt1+vt2+vt3+vt4+vt5


Rad_Vals=np.zeros((len(Seecool_volume[:,0,0])))
#Calculate total volume per toroidal index
for i in range(len(Seecool_volume[:,0,0])):
    for j in range(len(Seecool_volume[0,:,0])):
        for k in range(len(Seecool_volume[0,0,:])):
            Rad_Vals[i]+=my_volcel[i,j,k]*Seecool_volume[i,j,k]
##

print(phi[0:33].shape)
print(Rad_Vals.shape)
# Define x  axis for plot, since volume 'between' toroidal slices is calculated
# the mid point between toroidal angles is used as the x axis.
X=np.zeros((len(Seecool_volume[:,0,0])))
for i in range(len(Seecool_volume[:,0,0])):
    X[i]=Seecool.grid.phi_plane[i]+0.5

fig,ax=plt.subplots()
c=ax.pcolormesh(r[0,:,:],z[0,:,:],Seecool_volume[0,:,:],cmap='viridis_r',edgecolor='face')
ax.set_title(Seecool.grid.phi_plane[0])
ax.set_xlabel('Major Radius [cm]')
ax.set_ylabel('Vertical Direction [cm]')
fig.colorbar(c,ax=ax,label=r'Seecool [$\frac{W}{cm^3}$]')

fig,ax =plt.subplots()
ax.scatter(X, Rad_Vals)
ax.set_xlabel('Toroidal Angle')
ax.set_ylabel('Seecool [W]')
#ax.set_ylim(-190,0)
ax.grid()


#np.savetxt('V5_C_full_Rad_Vol_trace',Rad_Vals,delimiter=',')
#np.savetxt('Rad_dist_X',X,delimiter=',')
plt.show()
