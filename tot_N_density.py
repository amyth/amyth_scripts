from eelab import Simulation
import matplotlib.pyplot as plt
import numpy as np
from eelab.dtypes import VolData

# Plot the total nitrogen density accross all 5 valves and summed
# over all ionisation states  

s1=Simulation('./N2E13D.5_C3_V1_full')
s2=Simulation('./N2E13D.5_C3_V2_full')
s3=Simulation('./N2E13D.5_C3_V3_full')
s4=Simulation('./N2E13D.5_C3_V4_full')
s5=Simulation('./N2E13D.5_C3_V5_full')


niN1_1=s1['niN1']
niN1_2=s2['niN1']
niN1_3=s3['niN1']
niN1_4=s4['niN1']
niN1_5=s5['niN1']

niN2_1=s1['niN2']
niN2_2=s2['niN2']
niN2_3=s3['niN2']
niN2_4=s4['niN2']
niN2_5=s5['niN2']

niN3_1=s1['niN3']
niN3_2=s2['niN3']
niN3_3=s3['niN3']
niN3_4=s4['niN3']
niN3_5=s5['niN3']

niN4_1=s1['niN4']
niN4_2=s2['niN4']
niN4_3=s3['niN4']
niN4_4=s4['niN4']
niN4_5=s5['niN4']

niN5_1=s1['niN5']
niN5_2=s2['niN5']
niN5_3=s3['niN5']
niN5_4=s4['niN5']
niN5_5=s5['niN5']

niN6_1=s1['niN6']
niN6_2=s2['niN6']
niN6_3=s3['niN6']
niN6_4=s4['niN6']
niN6_5=s5['niN6']

niN7_1=s1['niN7']
niN7_2=s2['niN7']
niN7_3=s3['niN7']
niN7_4=s4['niN7']
niN7_5=s5['niN7']

niN1_1.values+=niN1_2.values+niN1_3.values+niN1_4.values+niN1_5.values
niN2_1.values+=niN2_2.values+niN2_3.values+niN2_4.values+niN2_5.values
niN3_1.values+=niN3_2.values+niN3_3.values+niN3_4.values+niN3_5.values
niN4_1.values+=niN4_2.values+niN4_3.values+niN4_4.values+niN4_5.values
niN5_1.values+=niN5_2.values+niN5_3.values+niN5_4.values+niN5_5.values
niN6_1.values+=niN6_2.values+niN6_3.values+niN6_4.values+niN6_5.values
niN7_1.values+=niN7_2.values+niN7_3.values+niN7_4.values+niN7_5.values

niN1_1.values+=niN2_1.values+niN3_1.values+niN4_1.values+niN5_1.values+niN6_1.values+niN7_1.values

niN1_1_12=niN1_1.rzslice(12)

niN1_1_36=niN1_1.rzslice(-35)

print('HERE', niN1_1_12.grid.u.shape)
print(niN1_1_12.grid.v.shape)
print(niN1_1_12.values.shape)

fig,ax=plt.subplots()
tmp=ax.pcolormesh(niN1_1_12.grid.u/100, niN1_1_12.grid.v/100, niN1_1_12.values)
cbar=plt.colorbar(tmp)
cbar.set_label(r'niN [$\frac{1}{cm^3}$]')
ax.set_title('Total Nitrogen Density')
ax.set_xlabel('Major Radius [m]')
ax.set_ylabel('Vertical Direction [m]')



fig,ax=plt.subplots()
tmp=ax.pcolormesh(niN1_1_36.grid.u/100, niN1_1_36.grid.v/100, niN1_1_36.values)
cbar=plt.colorbar(tmp)
cbar.set_label(r'niN [$\frac{1}{cm^3}$]')
ax.set_title('Total Nitrogen Density')
ax.set_xlabel('Major Radius [m]')
ax.set_ylabel('Vertical Direction [m]')

plt.show()
